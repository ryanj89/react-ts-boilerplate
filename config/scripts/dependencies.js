// Don't build DLL in production
if (process.env.NODE_ENV === 'production') {
  process.exit(0);
}

require('shelljs/global');

const { join } = require('path');
const fs = require('fs');
const exists = fs.existsSync;
const writeFile = fs.writeFileSync;

const defaults = require('lodash/defaultsDeep');
const pkgJson = require(join(process.cwd(), 'package.json'));
const config = require('../config');
const dllConfig = defaults(pkgJson.dllPlugin, config.dllPlugin.defaults);
const outputPath = join(process.cwd(), dllConfig.path);
const dllManifestPath = join(outputPath, 'package.json');

/**
 * Uses node_modules/react-ts-boilerplate-dlls by default since it will not be
 * version controlled and babel won't try to parse it.
 */
mkdir('-p', outputPath);

echo('Building webpack DLL...');

/**
 * Create DLL manifest so npm install doesn't warn us
 */
if (!exists(dllManifestPath)) {
  writeFile(
    dllManifestPath,
    JSON.stringify(
      defaults({
        name: 'react-ts-boilerplate-dlls',
        private: true,
        author: pkg.author,
        repository: pkg.repository,
        version: pkg.version,
      }),
      null,
      2,
    ),
    'utf8',
  );
}

// BUILDING_DLL environment variable is set to avoid confusing development environment
exec(
  'cross-env BUILDING_DLL=true webpack --display-chunks --color --config config/webpack/webpack.dllConfig.babel.js -- hide-modules',
);
