const chalk = require('chalk');

/**
 * Prints checkmark to console
 */
function addCheckmark(callback) {
  process.stdout.write(chalk.green(' ✓'));
  if (callback) {
    callback();
  }
}

module.exports = addCheckmark;
