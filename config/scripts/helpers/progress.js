const readline = require('readline');

/**
 * Adds animated progress indicator
 *
 * @param {string} message        Message to write
 * @param {number} ellipsisCount  Number of `.`'s to animate
 */
function animateProgress(message, ellipsisCount) {
  if (typeof ellipsisCount !== 'number') {
    ellipsisCount = 3;
  }

  let i = 0;
  return setInterval(() => {
    readline.cursorTo(process.stdout, 0);
    i = (i + 1) % (ellipsisCount + 1);
    const dots = new Array(i + 1).join('.');
    process.stdout.write(message + dots);
  }, 500);
}

module.exports = animateProgress;
